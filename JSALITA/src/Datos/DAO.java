/*
 * DAOSala.java
 *
 * Created on 19 de junio de 2006, 21:03
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package Datos;

import Negocio.*;
import java.util.List;
import java.util.Properties;
import org.hibernate.HibernateException;
import org.hibernate.*;
import org.hibernate.cfg.*;
import java.sql.DriverManager;
import org.hibernate.dialect.Dialect;
import org.hibernate.id.Configurable;
import org.hibernate.type.Type;

/**
 *
 * @author usuario
 */
public class DAO {
    
    private SessionFactory fabrica;
    private Session session=null;
    
    private Session getSession() {
        return session;
    }

    private void setSession(Session session) {
        this.session = session;
    }
    
    /** Creates a new instance of DAOSala */
    public DAO() throws HibernateException{
        System.out.println("creacion SessionFactory");
        DriverManager.setLogStream(System.out);
        try {
            fabrica=new Configuration().configure("HibernateMySQL.cfg.xml").buildSessionFactory();
            System.out.println("creacion SessionFactory");
        } catch(Exception ex){
           System.out.println(ex.getMessage());
           ex.printStackTrace(System.out);
           throw new HibernateException("Error al Conectarse a la Base de Datos");
        }
    }

   /** Inicializa la session para que comience la transaccion*/
    public void Inicializar(){
            if(getSession()==null){
                System.out.println("creacion Session");
                setSession(fabrica.openSession());
            }
    }
    
    /** Realiza Commit de las transacciones*/
    public void commit() throws Exception{
        try {
            getSession().beginTransaction().commit();            
        } catch(HibernateException ex){
            System.out.println(ex.getMessage());
            ex.printStackTrace(System.out);
            throw new Exception("Error en la base de Datos");
        }
    }
    
    /** Realiza Rollback de las transacciones*/
    public void rollback() {
         getSession().beginTransaction().rollback();                
    }
    
    public void close(){
        if (getSession()!=null){
            System.out.println("close Session");
            getSession().close();
            setSession(null);   
        }
    }
    
/** Guarda el objeto que le pasamos por parametros */ 
    public boolean guardar(Object object) throws Exception{
        boolean result=false;
        try{
            Inicializar();
            getSession().save(object);
            result=true;
        } catch(Exception ex){
            System.out.println(ex.getMessage());
            ex.printStackTrace(System.out);
            throw new Exception("Error al guardar "+object.toString());
        }
        return result;
    }

/** Elimina el objeto que le pasamos por parametros */ 
    public boolean eliminar(Object object) throws Exception{
        boolean result=false;
        try{
            Inicializar();
            getSession().delete(object); 
            result=true;
        } catch(Exception ex){
            System.out.println(ex.getMessage());
            ex.printStackTrace(System.out);
            throw new Exception("Error al guardar "+object.toString());
        } 
        return result;
    }
    
/** Modifica el <b>Objeto</b> que se pasa por parametros en la base de datos */
    public boolean update(Object object) throws Exception{
        boolean result=false;
        try {
            Inicializar();
            getSession().update(object);
            result =true;
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            ex.printStackTrace(System.out);
            throw new Exception("Error al modificar "+object.toString());
        } 
        return result;
    }
    
/** Toma de la base la lista de <b> Salas </b> y si no encuentra ninguna devuelve null*/
    public List getSalas() throws HibernateException{
        List result=null;
        try {
            Inicializar();
            Query query = getSession().createQuery("from Sala s");
            result=query.list();
        } catch (HibernateException ex) {
            System.out.println(ex.getMessage());
            ex.printStackTrace(System.out);
            throw new HibernateException("Error al consultar la base de Datos La Sala ");
        } catch (Exception ex){
            System.out.println(ex.getMessage());
            ex.printStackTrace(System.out);
        } 
        return result;
    }
    
/** Toma de la base la lista de <b> Salas </b> que pertenese al anio pasado por parametro y si no encuentra ninguna devuelve null*/
    public List getSalas(int anio) throws HibernateException{
        List result=null;
        try {
            Inicializar();
            Query query = getSession().createQuery("from Sala s where s.anio=:anio");
            query.setInteger("anio",anio);
            result=query.list();
        } catch (HibernateException ex) {
            System.out.println(ex.getMessage());
            ex.printStackTrace(System.out);
            throw new HibernateException("Error al consultar la base de Datos La Sala");
        } catch (Exception ex){
            System.out.println(ex.getMessage());
            ex.printStackTrace(System.out);
        } 
        return result;
    } 
    
/** Toma de la base el <b> Objeto </b> que tiene el ID pasado por parametro y si no encuentra ninguna devuelve null*/
    public Object get(Class aclass,Long ID) throws HibernateException{
        Object result=null;
        try {
            Inicializar();
            result = getSession().get(aclass,ID);
        } catch (HibernateException ex) {
            System.out.println(ex.getMessage());
            ex.printStackTrace(System.out);
            throw new HibernateException("Error al consultar la base de Datos "+aclass.toString());
        } catch (Exception ex){
            System.out.println(ex.getMessage());
            ex.printStackTrace(System.out);
        } 
        return result;
    }

/** Toma una Lista de Alumnos */
    public List getAlumnos() throws HibernateException{
        List result =null;
        try{
            Inicializar();
            result = getSession().createQuery("from Alumno").list();
        } catch (HibernateException ex) {
            System.out.println(ex.getMessage());
            ex.printStackTrace(System.out);
            throw new HibernateException("Error al consultar la base de Datos La Sala");
        } catch (Exception ex){
            System.out.println(ex.getMessage());
            ex.printStackTrace(System.out);
        }
        return result;
    }
    
/** Toma una Lista de Alumnos por el nombre o apellido pasado por parametros*/
    public List getAlumnos(String nombre,String apellido) throws HibernateException{
        List result =null;
        try{
            Inicializar();
            Query query = getSession().createQuery("from Alumno a where a.nombre=:nombre and a.apellido=:apellido");
            query.setString("nombre",nombre);
            query.setString("apellido",apellido);
            result=query.list();
        } catch (HibernateException ex) {
            System.out.println(ex.getMessage());
            ex.printStackTrace(System.out);
            throw new HibernateException("Error al consultar la base de Datos La Sala");
        } catch (Exception ex){
            System.out.println(ex.getMessage());
            ex.printStackTrace(System.out);
        }
        return result;
    }

/** Toma una Lista de Padres */
    public List getPadres() throws HibernateException{
        List result =null;
        try{
            Inicializar();
            result = getSession().createQuery("from Padre").list();
        } catch (HibernateException ex) {
            System.out.println(ex.getMessage());
            ex.printStackTrace(System.out);
            throw new HibernateException("Error al consultar la base de Datos La Sala");
        } catch (Exception ex){
            System.out.println(ex.getMessage());
            ex.printStackTrace(System.out);
        }
        return result;
    } 

/** Toma una Lista de Padres por el nombre o apellido pasado por parametros*/
    public List getPadres(String nombre,String apellido) throws HibernateException{
        List result =null;
        try{
            Inicializar();
            Query query = getSession().createQuery("from Padre a where a.nombre like :nombre and a.apellido like :apellido");
            query.setString("nombre",nombre);
            query.setString("apellido",apellido);
            result=query.list();
        } catch (HibernateException ex) {
            System.out.println(ex.getMessage());
            ex.printStackTrace(System.out);
            throw new HibernateException("Error al consultar la base de Datos La Sala");
        } catch (Exception ex){
            System.out.println(ex.getMessage());
            ex.printStackTrace(System.out);
        }
        return result;
    }    
    
/**Devuelve la cantidad de Salas*/
    public int cantidadSalas() throws HibernateException{
        int result =0;
        try{
            Inicializar();
            result =(Integer) (getSession().createQuery("select count(*) from Sala").uniqueResult());
        } catch (HibernateException ex) {
            System.out.println(ex.getMessage());
            ex.printStackTrace(System.out);
            throw new HibernateException("Error al consultar la base de Datos La Sala");
        } catch (Exception ex){
            System.out.println(ex.getMessage());
            ex.printStackTrace(System.out);
        } 
        return result;
    } 

/**Devuelve la cantidad de Salas del anio ingresado por parametro*/
    public int cantidadSalas(int anio) throws HibernateException, Exception{
        int result =0;
        try{
            Inicializar();
            Query query=getSession().createQuery("select count(*) from Sala s where s.anio=:anio");
            query.setInteger("anio",anio);
            result = ((Integer)query.uniqueResult()).intValue();
        } catch (HibernateException ex) {
            System.out.println(ex.getMessage());
            ex.printStackTrace(System.out);
            throw new HibernateException("Error al consultar la base de Datos La Sala");
        } catch (Exception ex){
            System.out.println(ex.getMessage());
            ex.printStackTrace(System.out);
            throw ex;
        }
        return result;
    } 

/**Lista los DNIs de los Alumnos que son parecidos parametro dni si no encuentra devuelve null*/
    public List ListarDNIdeAlumnos(String dni) throws HibernateException, Exception {
        List result =null;
        try{
            Inicializar();
            Query query=getSession().createQuery("select a.dni from Alumno a where a.dni like :dni");
            query.setString("dni",dni);
            result = query.list();
        } catch (HibernateException ex) {
            System.out.println(ex.getMessage());
            ex.printStackTrace(System.out);
            throw new HibernateException("Error al consultar la base de Datos La Sala");
        } catch (Exception ex){
            System.out.println(ex.getMessage());
            ex.printStackTrace(System.out);
            throw ex;
        } 
        return result;
    }

    /**recupera un alumno por su DNI*/
    public Alumno getAlumno(int dni) throws Exception {
        Alumno result =null;
        try{
            Inicializar();
            Query query=getSession().createQuery("select a from Alumno a where a.dni = :dni");
            query.setInteger("dni",dni);
            result = (Alumno)query.uniqueResult();
        } catch (HibernateException ex) {
            System.out.println(ex.getMessage());
            ex.printStackTrace(System.out);
            throw new HibernateException("Error al consultar la base de Datos La Sala");
        } catch (Exception ex){
            System.out.println(ex.getMessage());
            ex.printStackTrace(System.out);
            throw ex;
        } 
        return result;
    }

/**Lista los Alumnos que son parecidos parametro nombre si no encuentra devuelve null*/
    public List ListarAlumnosPorNombre(String nombre) throws Exception {
        List result =null;
        try{
            Inicializar();
            Query query=getSession().createQuery("select a from Alumno a where a.nombre like :nombre");
            query.setString("nombre",nombre);
            result = query.list();
        } catch (HibernateException ex) {
            System.out.println(ex.getMessage());
            ex.printStackTrace(System.out);
            throw new HibernateException("Error al consultar la base de Datos La Sala");
        } catch (Exception ex){
            System.out.println(ex.getMessage());
            ex.printStackTrace(System.out);
            throw ex;
        } 
        return result;
    }

/**Lista los Alumnos que son parecidos parametro apellido si no encuentra devuelve null*/
    public List ListarAlumnosPorApellido(String apellido) throws Exception {
        List result =null;
        try{
            Inicializar();
            Query query=getSession().createQuery("select a from Alumno a where a.apellido like :apellido");
            query.setString("apellido",apellido);
            result = query.list();
        } catch (HibernateException ex) {
            System.out.println(ex.getMessage());
            ex.printStackTrace(System.out);
            throw new HibernateException("Error al consultar la base de Datos La Sala");
        } catch (Exception ex){
            System.out.println(ex.getMessage());
            ex.printStackTrace(System.out);
            throw ex;
        } 
        return result;        
    }

    public List listarAlumnos(String nombre, String apellido) throws Exception,HibernateException {
       List result =null;
        try{
            Inicializar();
            Query query=getSession().createQuery("select a from Alumno a where a.apellido like :apellido and a.nombre like :nombre");
            query.setString("apellido",apellido);
            query.setString("nombre",nombre);
            result = query.list();
        } catch (HibernateException ex) {
            System.out.println(ex.getMessage());
            ex.printStackTrace(System.out);
            throw new HibernateException("Error al consultar la base de Datos La Sala");
        } catch (Exception ex){
            System.out.println(ex.getMessage());
            ex.printStackTrace(System.out);
            throw ex;
        } 
        return result;  
    }

    public List listarPadresPorDNI(String DNI) {
        List result =null;
        try{
            Inicializar();
            Query query=getSession().createQuery("select a from Padre a where a.dni like :dni");
            query.setString("dni",DNI);
            result = query.list();
        } catch (HibernateException ex) {
            System.out.println(ex.getMessage());
            ex.printStackTrace(System.out);
            throw new HibernateException("Error al consultar la base de Datos La Sala");
        } catch (Exception ex){
            System.out.println(ex.getMessage());
            ex.printStackTrace(System.out);
        }
        return result;
    }

    public Padre getPadres(int dni) throws Exception {
        Padre result =null;
        try{
            Inicializar();
            Query query=getSession().createQuery("select a from Padre a where a.dni = :dni");
            query.setInteger("dni",dni);
            result = (Padre)query.uniqueResult();
        } catch (HibernateException ex) {
            System.out.println(ex.getMessage());
            ex.printStackTrace(System.out);
            throw new HibernateException("Error al consultar la base de Datos La Sala");
        } catch (Exception ex){
            System.out.println(ex.getMessage());
            ex.printStackTrace(System.out);
            throw ex;
        } 
        return result;
    }

    public List getPadresPorNombre(String nombre) throws Exception {
        List result =null;
        try{
            Inicializar();
            Query query=getSession().createQuery("select a from Padre a where a.nombre like :nombre");
            query.setString("nombre",nombre);
            result = query.list();
        } catch (HibernateException ex) {
            System.out.println(ex.getMessage());
            ex.printStackTrace(System.out);
            throw new HibernateException("Error al consultar la base de Datos La Sala");
        } catch (Exception ex){
            System.out.println(ex.getMessage());
            ex.printStackTrace(System.out);
            throw ex;
        } 
        return result;
    }

    public List getPadresPorApellido(String apellido) throws Exception {
         List result =null;
        try{
            Inicializar();
            Query query=getSession().createQuery("select a from Padre a where a.apellido like :apellido");
            query.setString("apellido",apellido);
            result = query.list();
        } catch (HibernateException ex) {
            System.out.println(ex.getMessage());
            ex.printStackTrace(System.out);
            throw new HibernateException("Error al consultar la base de Datos La Sala");
        } catch (Exception ex){
            System.out.println(ex.getMessage());
            ex.printStackTrace(System.out);
            throw ex;
        } 
        return result;
    }

    public List ListarDNIdeAlumnosPorSala(String dni, Sala sala) throws Exception {
        List result =null;
        try{
            Inicializar();
            Query query=getSession().createQuery("select a.dni from Sala s inner join s.alumnos a " +
                                                 "where a.dni like :dni and s.id=:id");
            query.setString("dni",dni);
            query.setLong("id",sala.getId());
            result = query.list();
        } catch (HibernateException ex) {
            System.out.println(ex.getMessage());
            ex.printStackTrace(System.out);
            throw new HibernateException("Error al consultar la base de Datos La Sala");
        } catch (Exception ex){
            System.out.println(ex.getMessage());
            ex.printStackTrace(System.out);
            throw ex;
        } 
        return result;
    }

    public List ListarAlumnosPorNombrePorSala(String nombre, Sala sala) throws Exception {
        List result =null;
        try{
            Inicializar();
            Query query=getSession().createQuery("select a from Sala s inner join s.alumnos a " +
                                                  "where a.nombre like :nombre and s.id=:id");
            query.setString("nombre",nombre);
            query.setLong("id",sala.getId());
            result = query.list();
        } catch (HibernateException ex) {
            System.out.println(ex.getMessage());
            ex.printStackTrace(System.out);
            throw new HibernateException("Error al consultar la base de Datos La Sala");
        } catch (Exception ex){
            System.out.println(ex.getMessage());
            ex.printStackTrace(System.out);
            throw ex;
        } 
        return result;
    }

    public List ListarAlumnosPorApellidoPorSala(String apellido, Sala sala) throws Exception {
        List result =null;
        try{
            Inicializar();
            Query query=getSession().createQuery("select a from Sala s inner join s.alumnos a " +
                                                 "where a.apellido like :apellido and s.id=:id");
            query.setString("apellido",apellido);
            query.setLong("id",sala.getId());
            result = query.list();
        } catch (HibernateException ex) {
            System.out.println(ex.getMessage());
            ex.printStackTrace(System.out);
            throw new HibernateException("Error al consultar la base de Datos La Sala");
        } catch (Exception ex){
            System.out.println(ex.getMessage());
            ex.printStackTrace(System.out);
            throw ex;
        } 
        return result;   
    }

}
