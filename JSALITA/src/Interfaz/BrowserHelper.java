/*
* Copyright (c) 2003-2006 Fraunhofer IPSI, Darmstadt, Germany.
* Fraunhofer-Gesellschaft zur Förderung der angewandten Forschung e.V.
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
* - Redistributions of source code must retain the above copyright notice,
*   this list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 * - Neither the name of the Fraunhofer-Gesellschaft nor the names of its
 *   contributors may be used to endorse or promote products derived from this
 *   software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
 
/**
 * Author: guicking
 * Date:   29.05.2006
 * Time:   17:18:22
 * (c) Fraunhofer IPSI, CONCERT Division
 */
package Interfaz;
 
import java.lang.reflect.Method;
 
/**
 * Utility class to open a URL in the system default browser. Thieved from concert-base.
 *
 * @version $Id: BrowserHelper.java 5504 2006-09-28 09:45:32Z guicking $
 */
class BrowserHelper
{
        private BrowserHelper ()
        {
                // Avoid instantiation
        }
 
        /**
         * Thieved from concert-base: Opens a URL in the default browser.
         * <p/>
         * Tested with MacOSX 10.5.4 (Rike's PowerBook), MacOSX 10.2.8, WinXP, Win2000.
         *
         * @param url The URL to be opened in the browser.
         */
        static void openBrowser (String url)
        {
                String osName = System.getProperty("os.name");
                try
                {
                        if (osName.startsWith("Mac OS"))
                        {
                                Class fileMgr = Class.forName("com.apple.eio.FileManager");
                                Method openURL = fileMgr.getDeclaredMethod("openURL", new Class[]{String.class});
                                openURL.invoke(null, new Object[]{url});
                        } else if (osName.startsWith("Windows"))
                        {
                                Runtime.getRuntime().exec("rundll32 url.dll,FileProtocolHandler " + url);
                        } else
                        {
                                // Unix or Linux
                                String[] browsers = {"firefox", "opera", "konqueror", "epiphany", "mozilla", "netscape"};
                                String browser = null;
                                for (int count = 0; count < browsers.length && browser == null; count++)
                                {
                                        if (Runtime.getRuntime().exec(new String[]{"which", browsers[count]}).waitFor() == 0)
                                        {
                                                browser = browsers[count];
                                        }
                                }
                                if (browser == null)
                                {
                                        throw new Exception("Could not find web browser");
                                } else
                                {
                                        Runtime.getRuntime().exec(new String[]{browser, url});
                                }
                        }
                } catch (Exception e)
                {
                        throw new UnsupportedOperationException("Error attempting to launch web browser: " + e.getMessage());
                }
        }
}