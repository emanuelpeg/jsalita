/*
 * Configuracion.java
 *
 * Created on 16 de diciembre de 2007, 14:08
 *
 * Clase que se utiliza para guardar Las configuradiones de Interfaz
 */

package Interfaz;

import java.io.Serializable;


/**
 *
 * @author PEG
 */
public class Configuracion implements Serializable {
    
    /**Look and Feel */
    private String lookAndFeel = null;
    
    /**Theme */
    private String theme= null;
    
    /**
     * Creates a new instance of Configuracion
     */
    public Configuracion() {
    }

    public String getLookAndFeel() {
        return lookAndFeel;
    }

    public void setLookAndFeel(String lookAndFeel) {
        this.lookAndFeel = lookAndFeel;
    }

    public void setTheme(String theme) {
        this.theme=theme;
    }

    public String getTheme() {
        return theme;
    }

}
