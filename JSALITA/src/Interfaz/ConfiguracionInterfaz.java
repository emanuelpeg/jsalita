/*
 * ConfiguracionInterfaz.java
 *
 * Created on 5 de diciembre de 2007, 19:29
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package Interfaz;

import java.io.Serializable;
import javax.swing.plaf.basic.BasicLookAndFeel;

/**
 *
 * @author root
 * Clase singleton para guardar configuraciones visuales
 */
public class ConfiguracionInterfaz implements Serializable{
    
    private BasicLookAndFeel lookAndFeel= null;
    
    private static ConfiguracionInterfaz instancia=null;
    
    /** Creates a new instance of ConfiguracionInterfaz */
    private ConfiguracionInterfaz() {
    }
    
    public static ConfiguracionInterfaz getInstancia(){
        if (instancia == null){
            instancia= new  ConfiguracionInterfaz();
        }
        return instancia;
    }

    public BasicLookAndFeel getLookAndFeel() {
        return lookAndFeel;
    }

    public void setLookAndFeel(BasicLookAndFeel lookAndFeel) {
        this.lookAndFeel = lookAndFeel;
    }

    
}
