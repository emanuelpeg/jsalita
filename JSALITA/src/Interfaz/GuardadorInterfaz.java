/*
 * GuardadorInterfaz.java
 *
 * Created on 16 de diciembre de 2007, 14:40
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package Interfaz;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;


/**
 *
 * @author PEG
 */
public class GuardadorInterfaz {
    
   
    private File file=null;
    
    /** Creates a new instance of GuardadorInterfaz. Pide la direccion del archivo con
     *  que se va  a trabajar */
    public GuardadorInterfaz(String dir) {
         this.file=new File(dir);
    }
    
    
    /** Guarda el objeto de tipo Configuracion en archivo de path dado por la variable dir*/
    public void guardarConfiguracionInterfaz(Configuracion configuracion) throws FileNotFoundException, IOException, Exception{
        //Borra el archivo si existe
        this.file.delete();
        
        //Crea el archivo
        if (!this.file.createNewFile())
              throw new Exception("No se puede Crear el Archivo de Configuraci�n");
       
        file.setWritable(true);
        
        if (this.file.canWrite()){
            FileOutputStream fos=new FileOutputStream(this.file);
            ObjectOutputStream os= new ObjectOutputStream(fos);
        
            //Guarda el Objeto de Configuración
            os.writeObject(configuracion);   
        
            os.close();
            fos.close();
        } else {
            throw new Exception("No se puede Escribir el archivo");
        }
        
        
    }
    
    
    
    /** Recupera el objeto de tipo Configuracion en archivo de path dado por la variable dir*/
    public Configuracion getConfiguracionInterfaz() throws FileNotFoundException, IOException, ClassNotFoundException{
        
        FileInputStream fis= new FileInputStream(this.file);
        ObjectInputStream ois=new ObjectInputStream(fis);
        
        return (Configuracion)ois.readObject();
    }
    
    public boolean existConfig(){
        return this.file.exists();
    }
    
}
