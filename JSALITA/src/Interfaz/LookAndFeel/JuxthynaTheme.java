/*
 * PayasoTheme.java
 *
 * Created on 1 de diciembre de 2007, 16:14
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package Interfaz.LookAndFeel;

import java.awt.Font;
import javax.swing.plaf.ColorUIResource;
import javax.swing.plaf.FontUIResource;
import javax.swing.plaf.metal.OceanTheme;

/**
 *
 * @author root
 */
public class JuxthynaTheme extends OceanTheme{
    
    /**
     * Creates a new instance of PayasoTheme
     */
    public JuxthynaTheme() {
    }

    public String getName() {
        return "JuxthynaTheme";
    }

    protected ColorUIResource getPrimary1() {
        return new ColorUIResource(255,210,120);
    }

    protected ColorUIResource getPrimary2() {
        return new ColorUIResource(255,216,82);
    }

    protected ColorUIResource getPrimary3() {
        return new ColorUIResource(159,193,237);
    }

    protected ColorUIResource getSecondary1() {
        return new ColorUIResource(192,138,50);
    }

    protected ColorUIResource getSecondary2() {
        return new ColorUIResource(255,150,150);
    }

    protected ColorUIResource getSecondary3() {
        return new ColorUIResource(225,225,250);
    }

    public FontUIResource getControlTextFont() {
        return new FontUIResource("Lucida Sans",0,12); 
    }

    public FontUIResource getSystemTextFont() {
        return new FontUIResource("Lucida Sans",0,14);
    }

    public FontUIResource getUserTextFont() {
        return new FontUIResource("Lucida Sans",0,12);
    }

    public FontUIResource getMenuTextFont() {
        return new FontUIResource("Lucida Bright",0,13);
    }

    public FontUIResource getWindowTitleFont() {
        return new FontUIResource("Lucida Bright",0,14);
    }

    public FontUIResource getSubTextFont() {
        return new FontUIResource("Lucida Bright",0,12);
    }
    
    protected ColorUIResource getWhite(){
        return new ColorUIResource(235,235,255);
    }

    protected ColorUIResource getBlack() {
        return new ColorUIResource(35,35,55);
    }
    
    

    
}
