/*
 * PayasoTheme.java
 *
 * Created on 1 de diciembre de 2007, 16:14
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package Interfaz.LookAndFeel;

import java.awt.Font;
import javax.swing.plaf.ColorUIResource;
import javax.swing.plaf.FontUIResource;
import javax.swing.plaf.metal.MetalTheme;

/**
 *
 * @author root
 */
public class PayasoTheme extends MetalTheme{
    
    /**
     * Creates a new instance of PayasoTheme
     */
    public PayasoTheme() {
    }

    public String getName() {
        return "PayasoTheme";
    }

    protected ColorUIResource getPrimary1() {
        return new ColorUIResource(200,0,66);
    }

    protected ColorUIResource getPrimary2() {
        return new ColorUIResource(0,0,255);
    }

    protected ColorUIResource getPrimary3() {
        return new ColorUIResource(0,255,0);
    }

    protected ColorUIResource getSecondary1() {
        return new ColorUIResource(255,255,0);
    }

    protected ColorUIResource getSecondary2() {
        return new ColorUIResource(0,255,255);
    }

    protected ColorUIResource getSecondary3() {
        return new ColorUIResource(0,255,255);
    }

    public FontUIResource getControlTextFont() {
        return new FontUIResource("Lucida Sans",0,12); 
    }

    public FontUIResource getSystemTextFont() {
        return new FontUIResource("Lucida Sans",0,14);
    }

    public FontUIResource getUserTextFont() {
        return new FontUIResource("Lucida Sans",0,12);
    }

    public FontUIResource getMenuTextFont() {
        return new FontUIResource("Lucida Bright",0,13);
    }

    public FontUIResource getWindowTitleFont() {
        return new FontUIResource("Lucida Bright",0,14);
    }

    public FontUIResource getSubTextFont() {
        return new FontUIResource("Lucida Bright",0,12);
    }
    
}
