/*
 * SerNotificadoDeEleccion.java
 *
 * Created on 15 de agosto de 2006, 0:07
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package Interfaz;

import Negocio.Alumno;
import Negocio.Padre;

/**
 *
 * @author usuario
 */
public interface SerNotificadoDeEleccionPadre extends SerNotificado{
    
    void actulizarPorPadre(Padre padre);
    
}
