/*
 * SigletonVentana.java
 *
 * Created on 12 de marzo de 2005, 17:10
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package Interfaz;

import java.beans.PropertyVetoException;
import javax.swing.JInternalFrame;

/**
 *
 * @author Emanuel
 */
public class SigletonVentana extends JInternalFrame implements ObservadorAction {
    
    private static SigletonVentana instancia=null;
    
    private SerNotificado ObservadorDeResultado=null;
    
    private boolean FiltroSala=false;
    
    /** Creates a new instance of SigletonVentana */
    protected SigletonVentana() {
        
    }
  
    public static SigletonVentana getVentana(){
        if (getInstancia()==null){
            setInstancia(new SigletonVentana());
        } 
        return getInstancia();
    }
    
    public void serNotificadoAction() {
        try {
            this.setSelected(true);
        } catch (PropertyVetoException ex) {
            ex.printStackTrace();
        }
    }

    public void serNotificadoClosed() {
        this.setVisible(false);
    }


    protected static SigletonVentana getInstancia() {
        return instancia;
    }

    protected static void setInstancia(SigletonVentana aInstancia) {
        instancia = aInstancia;
    }
    
   

    public void setVisible(boolean aFlag) {
        super.setVisible(aFlag);
        if (!aFlag){
            setInstancia(null);
        }
    }

    public SerNotificado getObservadorDeResultado() {
        return ObservadorDeResultado;
    }

    public void setObservadorDeResultado(SerNotificado ObservadorDeResultado) {
        this.ObservadorDeResultado = ObservadorDeResultado;
    }

    void setDniSeleccionado(int dni) {
       
    }

    void setFiltroSala(boolean b) {
        FiltroSala=true;
    }

    protected boolean isFiltroSala() {
        return FiltroSala;
    }

    
}
