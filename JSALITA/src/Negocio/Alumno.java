/*
 * Alumno.java
 *
 * Created on 10 de junio de 2006, 1:54
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package Negocio;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 *
 * @author Goette
 */
public class Alumno extends Persona{
    
    /*ATRIBUTOS*/
   
    /**Lista de los padres del alumno*/
    private Set<Padre> padres=new HashSet<Padre>();
   
    /**Descripción del parto del Alumno*/
    private String parto;
   
    /**Observacion sobre enfermedad del alumno*/
    private String enfermedad;
     
    /**Lista de las salas que fue en cada año*/
    private Map<Integer,Aula> aulas =new HashMap<Integer,Aula>();
    
    /**Año que esta cursando propiedad no persistente*/
    private Integer anio=new Integer(0);
    
    /*METODOS*/
    /** Creates a new instance of Alumno para que lo utilice hibernate*/
    public Alumno() {
    }
    
    /** Creador a utilizar*/
    public Alumno(int dni,String nombre,String apellido,Date fechaNacimiento,String direccion,char sexo){
        super(dni,nombre,apellido,fechaNacimiento,direccion,sexo);
    }

    public Date getFechaSecretario() {
        return this.getAulas().get(this.getAnio()).getFechaSecretario();
    }

    public void setFechaSecretario(Date fechaSecretario) {
        Date fecha = new Date(fechaSecretario.getYear(),fechaSecretario.getMonth(),fechaSecretario.getDate(),0,0,0);
        this.getAulas().get(this.getAnio()).setFechaSecretario(fecha);
    }

    public Set<Padre> getPadres() {
        return padres;
    }

    /**
     * 
     * @param padres 
     */
    public void setPadres(Set<Padre> padres) {
        this.padres = padres;
    }

    public String getParto() {
        return parto;
    }

    public void setParto(String parto) {
        this.parto = parto;
    }

    public String getEnfermedad() {
        return enfermedad;
    }

    public void setEnfermedad(String enfermedad) {
        this.enfermedad = enfermedad;
    }

    public Set<Date> getFaltas() {
        return this.getAulas().get(this.getAnio()).getFaltas();
    }

    public void setFaltas(Set<Date> faltas) {
        this.getAulas().get(this.getAnio()).setFaltas(faltas);
    }

    public Set<Nota> getNotas() {
        return this.getAulas().get(this.getAnio()).getNotas();
    }

    public void setNotas(Set<Nota> notas) {
        this.getAulas().get(this.getAnio()).setNotas(notas);
    }
    
    /**Agregar una falta*/
    public void addFalta(Date dia){
        dia=new Date(dia.getYear(),dia.getMonth(),dia.getDate(),0,0,0);
        this.getFaltas().add(dia);
    }
    
    public boolean removerFalta(Date dia){
        return this.getFaltas().remove(dia);
    }
    
    /**Procedimiento que indica si falto el alumno*/
    public boolean falte(Date fecha){
        fecha=new Date(fecha.getYear(),fecha.getMonth(),fecha.getDate(),0,0,0);
        boolean result=false;
        Set<Date> faltas= this.getAulas().get(this.getAnio()).getFaltas();
        for (Date date : faltas) {
            if (date.equals(fecha)){
                result=true;
            }
        }
        return result;
    }
    
    /**Devuelve el promedio del alumno*/
    public double promedio(){
        double result=0;
        Set<Nota> notas=this.getAulas().get(this.getAnio()).getNotas();
        for (Nota nota : notas) {
            result+=nota.getNota();
        }
        return (result/notas.size());
    }

    public String toString() {
        return this.getNombre()+"  "+this.getApellido();
    }

    public Sala getSala() {
        return this.getAulas().get(this.getAnio()).getSala();
    }

    public void setSala(Sala sala) throws Exception {
        if (sala.getAnio()>this.getAnio()){
            Aula aula=new Aula(sala);
            this.getAulas().put(sala.getAnio(),aula);
            sala.getAlumnos().add(this);
            calcularAnio();
        } else {
            cambiarseSala(sala);
        }
    }

    public Map<Integer, Aula> getAulas() {
        return aulas;
    }

    public void setAulas(Map<Integer, Aula> aulas) {
        this.aulas = aulas;
        calcularAnio();
    }

    /**Metodo que toma el año que esta cursando, siempre sera el mayor*/
    public Integer getAnio(){
           return anio; 
    }
    
    /**Metodo para cambiarse de sala */
    public void cambiarseSala(Sala sala) throws Exception{
        if (sala.getAnio()==this.getSala().getAnio()){
            this.getSala().getAlumnos().remove(this);
            this.getAulas().get(this.getAnio()).setSala(sala);
            sala.getAlumnos().add(this);
        } else {
            throw new Exception("El cambio de sala debe ser entre salas del mismo año");
        }
    }
    
    private void calcularAnio(){
        Integer max =0;
        Set<Integer> anios=this.getAulas().keySet();
        for(Integer ano:anios){
            if (ano>max){
                max=ano;
            }
        }
        anio= max;
    }

    public int getEdad() {
        Date d=new Date();
        d.setYear(this.getFechaNacimiento().getYear());
        int resultado = new Date().getYear() - this.getFechaNacimiento().getYear();
        if (d.compareTo(this.getFechaNacimiento())==-1){
            resultado --;
        }
        return resultado;
    }
   
}
