/*
 * Aula.java
 *
 * Created on 17 de septiembre de 2006, 13:48
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package Negocio;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author PEG
 */
public class Aula {
    
    /*ATRIBUTOS*/    
    /**Nro para utilizar hibernate*/
    private Long id;
    
    /**lista de fechas en las que el alumno falto*/
    private Set<Date> faltas=new HashSet<Date>();
    
    /**Lista de notas */
    private Set<Nota> notas=new HashSet<Nota>();
    
    /**Salita que asiste el alumno*/
    private Sala sala=null;
    
    /**Ultima vez que fue secretario para poder elegir el secretario
     *si la feca de secretario es la actual es porque es secretario*/
    private Date fechaSecretario=new Date();
       
    /** Creates a new instance of Aula para hibernate*/
    public Aula() {
    }

    /** creates a utilizar*/
    public Aula(Sala sala){
        this.setSala(sala);
    }
    
    public Set<Date> getFaltas() {
        return faltas;
    }

    public void setFaltas(Set<Date> faltas) {
        this.faltas = faltas;
    }

    public Set<Nota> getNotas() {
        return notas;
    }

    public void setNotas(Set<Nota> notas) {
        this.notas = notas;
    }

    public Sala getSala() {
        return sala;
    }

    public void setSala(Sala sala) {
        this.sala = sala;
    }

    public Date getFechaSecretario() {
        return fechaSecretario;
    }

    public void setFechaSecretario(Date fechaSecretario) {
        this.fechaSecretario = fechaSecretario;
    }
   
    /**Agregar una falta*/
    public void addFalta(Date dia){
        this.getFaltas().add(dia);
    }
    
    public boolean removerFalta(Date dia){
        return this.getFaltas().remove(dia);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    
}
