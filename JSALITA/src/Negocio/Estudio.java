/*
 * Estudios.java
 *
 * Created on 6 de agosto de 2006, 14:36
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package Negocio;

/**
 *
 * @author PEG
 */
public class Estudio {
    
    /** Id de Estudios para Hibernate*/
    private Long id;
    
    /** Titulo*/
    private String titulo; 
    
    /** Tipo de Estudio */
    private TipoEstudio tipo;
    
    /** Observaciòn*/
    private String observacion;
    
    /** Creates a new instance of Estudios */
    public Estudio() {
    }

    public Estudio(String titulo,TipoEstudio tipo, String observacion){
        this.setTitulo(titulo);
        this.setTipo(tipo);
        this.setObservacion(observacion);
    }
    
    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public TipoEstudio getTipo() {
        return tipo;
    }

    public void setTipo(TipoEstudio tipo) {
        this.tipo = tipo;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long idEstudios) {
        this.id = idEstudios;
    }

    public String toString() {
      return titulo;
    }
    

    
}
