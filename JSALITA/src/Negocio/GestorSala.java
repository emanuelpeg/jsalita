/*
 * GestorSala.java
 *
 * Created on 13 de febrero de 2005, 14:03
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package Negocio;

import Datos.*;
import DatosHelper.Helper;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;


/**
 *
 * @author Emanuel
 */
public class GestorSala {
    
    /** Instancia para implementar el singleton*/
    private static GestorSala instancia=null;
    
    /**Sala elegida*/
    private Sala sala=null;
    
    /** DAO para recuperar, guardar y actulizar datos*/
    private DAO dao=new DAO();
    
    /** Creates a new instance of GestorSala */
    private GestorSala() {
    }
    
    public static GestorSala getInstancia(){
        if (instancia==null){
            instancia=new GestorSala();
        }
        return instancia;
    }
    
    public void elegirSala(Sala sala){
        this.sala=sala;
    }
    
    public boolean salaElegida(){
        return (this.getSala()!=null);
    }
    
    public List listarSalasanio() throws Exception {
        Date hoy=new Date();
        List result =dao.getSalas(hoy.getYear()+1900);
        return result;
    }
    
    public List listarSalasanio(int anio) throws Exception {
        List result =dao.getSalas(anio);
        return result;
    }
    
    
    public List listarSalas() throws Exception {
        List result =dao.getSalas();
        return result;
    }
    
    public int cantidadSalas() throws Exception{
        return dao.cantidadSalas();
    }

    public Sala getSala() {
        return sala;
    }
    
    public void close(){
        dao.close();
    }
    
    public boolean guardar(Object obj) throws Exception{
        boolean result;
        try{
            result= dao.guardar(obj);
            dao.commit();
        } catch (Exception ex){
            dao.rollback();
            throw ex;
        }
        return result;
    }
    
    public boolean eliminar(Object obj) throws Exception{
        boolean result;
        try{
            result=dao.eliminar(obj);
            dao.commit();
        } catch (Exception ex){
            dao.rollback();
            throw ex;
        }
        return result;
    }
    
    public boolean modificar(Object obj) throws Exception{
        boolean result;
        try{
            result=dao.update(obj);
            dao.commit();
        } catch (Exception ex){
            dao.rollback();
            throw ex;
        }
        return result;
    } 
    
    public boolean guardarSinCommit(Object obj) throws Exception{
        return dao.guardar(obj);
    }
    
    public boolean eliminarSinCommit(Object obj) throws Exception{
        return dao.eliminar(obj);
    }
    
    public boolean modificarSinCommit(Object obj) throws Exception{
        return dao.update(obj); 
    } 
    
    public void commit() throws Exception{
        dao.commit();
    }
    
    public void rollback(){
        dao.rollback();
    }
    
    public List listarPadres() throws Exception {
        List result =dao.getPadres();
        return result;
    }
    
    private String validarParametroQuery(String parametro){
        return Helper.validarParametroQuery(parametro);
    }
    
    public List listarPadres(String nombre,String apellido) throws Exception {
        nombre=validarParametroQuery(nombre);
        apellido=validarParametroQuery(apellido);
        List result =dao.getPadres(nombre,apellido);
        return result;
    }

    public List ListarDNIdeAlumnos(String dni) throws Exception{      
        return dao.ListarDNIdeAlumnos(dni);
    }

    public Alumno getAlumno(int dni) throws Exception {
        return dao.getAlumno(dni);
    }

    public List ListarAlumnosPorNombre(String nombre) throws Exception {
        return dao.ListarAlumnosPorNombre(nombre);
    }

    public List ListarAlumnosPorApellido(String apellido) throws Exception {
        return dao.ListarAlumnosPorApellido(apellido);
    }

    public void cambiarAlumnoSala(Alumno alumno, Sala sala) throws Exception {
       try{
           if (alumno.getAnio()==sala.getAnio()){
               Sala aux=alumno.getSala();
               alumno.setSala(sala);
               dao.update(aux);
               dao.update(alumno.getAulas().get(alumno.getAnio()));
           } else {
               alumno.setSala(sala);
               dao.guardar(alumno.getAulas().get(alumno.getAnio()));
           }
           dao.update(alumno);
           dao.update(sala); 
           dao.commit();
       } catch(Exception ex){
           dao.rollback();
       }
                
    }

    
    public void tomarAsistencia(Map<Alumno,Boolean> asistencias, Date dia) throws Exception {
        dia.setHours(0);
        dia.setMinutes(0);
        dia.setSeconds(0);
        if (!this.getSala().esDiaDeClase(dia)){
            this.getSala().getDiasClase().add(dia);
        }
        Set<Alumno> alumnos=asistencias.keySet();
        for(Alumno alumno:alumnos){
            if (!asistencias.get(alumno)){
                this.getSala().ausente(alumno,dia);
                this.dao.update(alumno);
            } else {
                if (this.getSala().removerFalta(alumno,dia)) {
                    this.dao.update(alumno);
                }
            }
        }
        dao.update(this.getSala());
        dao.commit();
    }

    public void addFaltas(Alumno alumno, Set<Date> dias) throws Exception {
        for(Date dia:dias){
            this.getSala().ausente(alumno,dia);
            this.dao.update(alumno);
        }
        dao.update(this.getSala());
        dao.commit();
    }

    public void eliminarFaltas(Date date) throws Exception {
        this.getSala().removerDiaDeClase(date);
        dao.update(this.getSala());
        dao.commit();
    }

    public List listarAlumnos(String nombre, String apellido) throws Exception {
        return dao.listarAlumnos(nombre,apellido);
    }

    public Alumno elegirSecretario(Date date) {
        Alumno resultado=this.getSala().elegirSecretario(date);
        return resultado;        
    }

    public void guardarSecretarios(Date date) throws Exception {
        for(Alumno alumno : this.getSala().getAlumnos()){
            if (alumno.getFechaSecretario().equals(date)){
                this.guardar(alumno);
            }
        }
    }

    public List ListarPadresPorDNI(String DNI) {
        return dao.listarPadresPorDNI(DNI);
    }

    public Padre getPadre(int dni) throws Exception {
        return dao.getPadres(dni);
    }

    public List ListarPadresPorNombre(String nombre) throws Exception {
        return dao.getPadresPorNombre(nombre);
    }

    public List ListarPadresPorApellido(String apellido) throws Exception {
        return dao.getPadresPorApellido(apellido);
    }

    public List ListarDNIdeAlumnosPorSala(String dni) throws Exception {
        return dao.ListarDNIdeAlumnosPorSala(dni, this.getSala());
    }

    public List ListarAlumnosPorNombrePorSala(String nombre) throws Exception {
        return dao.ListarAlumnosPorNombrePorSala(nombre,this.getSala());
    }

    public List ListarAlumnosPorApellidoPorSala(String apellido) throws Exception {
        return dao.ListarAlumnosPorApellidoPorSala(apellido,this.getSala());
    }



    
}
