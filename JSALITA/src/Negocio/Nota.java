/*
 * Nota.java
 *
 * Created on 10 de junio de 2006, 2:02
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package Negocio;

import java.util.Date;

/**
 *
 * @author Goette
 */
public class Nota {
    
    /**id de Nota que lo usa hibernate*/
    private Long id;
    /**Nota*/
    private int nota;
    /**Observacion */
    private String observacion;
    /**Fecha de Nota*/
    private Date fecha;

    /** Creates a new instance of Nota para que lo utilice hibernate*/
    public Nota() {
    }

    /**Creador a utilizar*/
    public Nota(int nota,String observacion,Date fecha){
        this.setNota(nota);
        this.setObservacion(observacion);
        this.setFecha(fecha);
    }
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getNota() {
        return nota;
    }

    public void setNota(int nota) {
        this.nota = nota;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }
    
}
