/*
 * Padre.java
 *
 * Created on 10 de junio de 2006, 1:51
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package Negocio;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author Goette
 */
public class Padre extends Persona{
    
    /*ATRIBUTOS*/
    /**Lista de Hijos del padre **/
    private Set<Alumno> hijos=new HashSet<Alumno>();
    
    /**Lista de Estudios*/
    private Set<Estudio> estudios=new HashSet<Estudio>();
    
    /**Lista de trabajos*/
    private Set<Trabajo> trabajo=new HashSet<Trabajo>();
    
    /**Conyuge*/
    private Set<Padre> conyuge=new HashSet<Padre>();
    
    /** Creates a new instance of Padre lo utiliza hibernate*/
    public Padre() {
    }

    /*METODOS*/
    /**Creador a utilizar*/
    public Padre(int dni,String nombre,String apellido,Date fechaNacimiento,String direccion,char sexo){
        super(dni,nombre,apellido,fechaNacimiento,direccion,sexo);
    }
    
    public Set<Alumno> getHijos() {
        return hijos;
    }

    public void setHijos(Set<Alumno> hijos) {
        this.hijos = hijos;
    }

    public Set<Estudio> getEstudios() {
        return estudios;
    }

    public void setEstudios(Set<Estudio> estudios) {
        this.estudios = estudios;
    }

    public Set<Trabajo> getTrabajo() {
        return trabajo;
    }

    public void setTrabajo(Set<Trabajo> trabajo) {
        this.trabajo = trabajo;
    }

    public Set<Padre> getConyuge() {
        return conyuge;
    }

    public void setConyuge(Set<Padre> conyuge) {
        this.conyuge = conyuge;
    }

    
}
