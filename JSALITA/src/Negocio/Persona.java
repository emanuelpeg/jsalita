/*
 * Persona.java
 *
 * Created on 10 de junio de 2006, 1:27
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package Negocio;

import java.util.Date;

/**
 *
 * @author Goette
 */
public abstract class Persona {
    /*CONSTANTES*/
    /**Sexo masculino*/
    public final static char MASCULINO='M';
    /**Sexo femenino*/
    public final static char FEMENINO='F'; 
    
    /*ATRIBUTOS*/
    /**Nro para utilizar hibernate*/
    protected Long id;
    /**DNI de Persona*/
    protected int dni;
    /**Nombre de Persona*/
    protected String nombre;
    /**Apellido de Persona*/
    protected String apellido;
    /**Fecha de Nacimiento de Persona*/
    protected Date fechaNacimiento;
    /**Direcciòn de Persona*/
    protected String direccion;
    /**Religion de Persona*/
    protected String religion;
    /**Sexo de Persona*/
    protected char sexo;
    /**Telefono de Persona*/
    protected String telefono;

    
    /*METODOS*/
    /** Creates a new instance of Persona  para que lo utilice hibernate*/
    public Persona() {
    }

    /**Creador a utilizar*/
    public Persona(int dni,String nombre,String apellido,Date fechaNacimiento,String direccion,char sexo){
        this.setDni(dni);
        this.setNombre(nombre);
        this.setApellido(apellido);
        this.setFechaNacimiento(fechaNacimiento);
        this.setDireccion(direccion);
        this.setSexo(sexo);        
    }
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getDni() {
        return dni;
    }

    public void setDni(int dni) {
        this.dni = dni;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public Date getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(Date fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getReligion() {
        return religion;
    }

    public void setReligion(String religion) {
        this.religion = religion;
    }

    public char getSexo() {
        return sexo;
    }

    public void setSexo(char sexo) {
        this.sexo = sexo;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }
    
    /** overrive toString */
    public String toString(){
        return this.getNombre()+" "+this.getApellido()+" ";
    }
}
