/*
 * Sala.java
 *
 * Created on 12 de junio de 2006, 0:58
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package Negocio;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.Vector;

/**
 *
 * @author usuario
 */
public class Sala {
    /**Id de salita para usar hibernate*/
    private Long id;
    /**Nombre de la Salita */
    private String nombre;
    /**Letra de la salita*/
    private char letra;
    /**Anio electivo*/
    private int anio;
    /**Lista de alumnos que cursan en la salita*/
    private Set<Alumno> alumnos=new HashSet<Alumno>();
    /**Lista de dias de Clase*/
    private Set<Date> diasClase =new HashSet<Date>();
    
    /** Creates a new instance of Sala se usa para hibernate*/
    public Sala() {
    }

    /** Constructor */
    public Sala(String nombre,char letra,int anio){
        this.setNombre(nombre);
        this.setLetra(letra);
        this.setAnio(anio);
    }
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public char getLetra() {
        return letra;
    }

    public void setLetra(char letra) {
        this.letra = letra;
    }

    public int getAnio() {
        return anio;
    }

    public void setAnio(int anio) {
        this.anio = anio;
    }

    /** Devuelve los alumnos de la Sala si el alumno esta actualmente en la sala */
    public Set<Alumno> getAlumnosAnual() {
        Set<Alumno> resultado = new HashSet<Alumno>();
        for (Alumno a : this.getAlumnos() ){
            if (a.getSala().equals(this)){
                resultado.add(a);
            }
        }
        return resultado;
    }

    public void setAlumnos(Set<Alumno> alumnos) {
        this.alumnos = alumnos;
    }

    public Set<Date> getDiasClase() {
        return diasClase;
    }

    public void setDiasClase(Set<Date> diasClase) {
        this.diasClase = diasClase;
    }
    /**Saca el promedio general de la Sala*/
    public double promedio(){
        double acum=0;
        for(Alumno alumno :alumnos){
            acum=alumno.promedio()+acum;
        }
        return acum/alumnos.size();
    }
    
    /**Devuelve la fecha más antigua en la que fue secretario un alumno*/
    private Date fechaAntiguaSecretario(){
        Date antigua=new Date();
        for (Alumno alumno : alumnos){
            if (antigua.before(alumno.getFechaSecretario())){
                antigua=alumno.getFechaSecretario();
            }
        }
        return antigua;
    }
    
    /**Devuelve una lista de alumnos que fueron secretarios el dia pasado por parametro*/
    private ArrayList<Alumno> listaSecretariosPorFecha(Date dia){
        ArrayList<Alumno> result = new ArrayList<Alumno>();
        for(Alumno alumno : alumnos){
            if(alumno.getFechaSecretario().equals(dia)){
                result.add(alumno);
            }
        }
        return result;
    }
    
    /**Elige el secretario para lo cual se utiliza las funciones para sacar los 
     * alumnos que hace mayor tiempo que no son secretarios. Funcion FechaAntiguaSecretario
     * y listaSecretariosPorFecha */ 
    public Alumno elegirSecretario(Date dia){
       if(this.getAlumnos().size()>0){
        ArrayList<Alumno> alumnosElegir=this.listaSecretariosPorFecha(this.fechaAntiguaSecretario());
        Random random= new Random();
        Alumno alumno=null;
        if (alumnosElegir.size()==0){
            alumno= (Alumno) this.getAlumnos().toArray()[random.nextInt(this.getAlumnos().size())];
        } else {
            alumno=alumnosElegir.get(random.nextInt(alumnosElegir.size()));
        }
        Date date=alumno.getFechaSecretario();
        HashSet<Alumno> aux=this.listaPresentes(dia);
        if (aux.contains(alumno)){
            if ((date.getDate()==dia.getDate())&&(date.getMonth()==dia.getMonth())&&(dia.getYear()==date.getYear())){
              //TODO : ver si ya es secretario
               alumno=(Alumno) aux.toArray()[random.nextInt(aux.size())];
            } 
        } else {
            if (aux.size()>0){
                alumno=(Alumno) aux.toArray()[random.nextInt(aux.size())];   
            } else {
                alumno= null;
            }             
        }
        if (alumno!=null) {
            alumno.setFechaSecretario(dia);
        }
        return alumno;
      } else {
           return null;
      }
  }
    
    /**Coloca el ausente al alumno cuando falto*/
    public boolean ausente(Alumno palumno, Date dia){
        dia=new Date(dia.getYear(),dia.getMonth(),dia.getDate(),0,0,0);
        boolean result=false;
        for(Alumno alumno:alumnos){
            if (alumno.equals(palumno)){
                alumno.getFaltas().add(dia);
                result=true;
                break;
            }
        }
        return result;
    }
    
    /**Elimina de la lista de fechas que falto el alumno el dia pasado por parametro
     devuelve verdadero si puede y falso si no puede*/
    public boolean removerFalta(Alumno palumno, Date dia){
        dia=new Date(dia.getYear(),dia.getMonth(),dia.getDate(),0,0,0);
        boolean result=false;
        for(Alumno alumno:alumnos){
            if (alumno.equals(palumno)){
                result=alumno.getFaltas().remove(dia);
                break;
            }
        }
        return result;
    }
    
    /**Coloca la nota al alumno*/
    public boolean calificar(Alumno palumno, Nota nota){
        boolean result=false;
        for(Alumno alumno:alumnos){
            if (alumno.equals(palumno)){
                alumno.getNotas().add(nota);
                result=true;
                break;
            }
        }
        return result;
    }
    
    /**Elimina una nota del alumno que se haya ingresado por error*/
    public boolean removerNota(Alumno palumno, Nota nota){
        boolean result=false;
        for(Alumno alumno:alumnos){
            if(alumno.equals(palumno)){
                result=alumno.getNotas().remove(nota);
                break;
            }
        }
        return result;
    }
    
    /**Listar los presentes*/
    public HashSet<Alumno> listaPresentes(Date dia){
          boolean aux;
          HashSet<Alumno> result=new HashSet<Alumno>();
          for(Alumno alumno:alumnos){
              aux=true;
              for(Date date:alumno.getFaltas()){
                  if((date.getDate()==dia.getDate())&&(date.getMonth()==dia.getMonth())&&(dia.getYear()==date.getYear())){
                      aux=false;
                      break;
                  }
              }
              if(aux){
                  result.add(alumno);
              }
          }
          return result;
    }
    
    /**Lista los Ausentes*/
    public HashSet<Alumno> listarAusentes(Date dia){
        dia.setMinutes(0);
        dia.setSeconds(0);
        dia.setHours(0);
        HashSet<Alumno> result=new HashSet<Alumno>();
        for(Alumno alumno:alumnos){
            for(Date date:alumno.getFaltas()){
                if ((date.getDate()==dia.getDate())&&(date.getMonth()==dia.getMonth())&&(dia.getYear()==date.getYear())){
                    result.add(alumno);
                    break;
                }
            }
        }
        return result;
    }
    
/**Retorna true si es un dia de clase y falso si no el dia ingrasado por parametro */    
    public boolean esDiaDeClase(Date dia){
         boolean resultado=false;
         for (Date diaux: this.getDiasClase()){
             if ((diaux.getDay()==dia.getDay())&&(diaux.getMonth()==dia.getMonth())&&(diaux.getYear()==dia.getYear())){
                 resultado =true;
                 break;
             }
         }
         return resultado;
    }
    
    public String toString() {
        return this.getNombre();
    }

    public boolean removerDiaDeClase(Date date) {
        boolean resultado = this.getDiasClase().remove(date);
        for (Alumno alumno: this.getAlumnos()){
            alumno.removerFalta(date);
        }
        return resultado;
    }

    /**Devuleve una coleccion de vectores de alumnos faltantes por dia*/
    public Collection<Vector> listarAusentes() {
        Vector row=new Vector();
        Collection<Vector> matriz=new ArrayList<Vector>();
        Set<Date> dias=this.getDiasClase();
        Set<Alumno> alumnos;
        java.text.DateFormat df=new java.text.SimpleDateFormat("dd-MMM-yyyy");
        for(Date dia:dias){
            alumnos=this.listarAusentes(dia);
            for(Alumno alumno:alumnos){
                row.add(alumno.getDni());
                row.add(alumno.getNombre());
                row.add(alumno.getApellido());
                row.add(df.format(dia));
                matriz.add(row);
                row=new Vector();
            }
        }
        return matriz;
    }
/** Función que retorna el primer dia de clase  si no existen dias de clase devuelve null*/
    public Date getPrimerDiaClase() {
       Date min=null;
       try {
           min=this.diasClase.iterator().next();
           for (Date d:this.diasClase){
            if (d.before(min)){
                min=d;
            }
           }
       } catch (Exception ex){
           ex.printStackTrace();
       }
       return min;
    }
    
/** Función que retorna el ultimo dia de clase  si no existen dias de clase devuelve null*/
    public Date getUltimoDiaClase() {
       Date max=null;
       try {
           max=this.diasClase.iterator().next();
           for (Date d:this.diasClase){
            if (d.after(max)){
                max=d;
            }
           }
       } catch (Exception ex){
           ex.printStackTrace();
       }
       return max;
    }

    public Collection listarAusentes(Date primerDia, Date ultimoDia) {
        Vector row=new Vector();
        Collection<Vector> matriz=new ArrayList<Vector>();
        Set<Date> diasT=this.getDiasClase();
        Set<Date> dias=new HashSet<Date>();
        for(Date d:diasT){
            if (d.before(ultimoDia)&&(d.after(primerDia)))
            dias.add(d);
        }
        dias.add(primerDia);
        dias.add(ultimoDia);
        Set<Alumno> alumnos; 
        java.text.DateFormat df=new java.text.SimpleDateFormat("dd-MMM-yyyy");
        for(Date dia:dias){
            alumnos=this.listarAusentes(dia);
            for(Alumno alumno:alumnos){
                row.add(alumno.getDni());
                row.add(alumno.getNombre());
                row.add(alumno.getApellido());
                row.add(df.format(dia));
                matriz.add(row);
                row=new Vector();
            }
        }
        return matriz;
    }

    public Collection listarAusentes(Date primerDia, Date ultimoDia, Alumno alumno) {
        Vector row=new Vector();
        Collection<Vector> matriz=new ArrayList<Vector>();
        Set<Date> diasT=this.getDiasClase();
        Set<Date> dias=new HashSet<Date>();
        for(Date d:diasT){
            if (d.before(ultimoDia)&&(d.after(primerDia)))
            dias.add(d);
        }
        dias.add(primerDia);
        dias.add(ultimoDia);
        java.text.DateFormat df=new java.text.SimpleDateFormat("dd-MMM-yyyy");
        for(Date dia:dias){
           if (this.listarAusentes(dia).contains(alumno)) {
                row.add(alumno.getDni());
                row.add(alumno.getNombre());
                row.add(alumno.getApellido());
                row.add(df.format(dia));
                matriz.add(row);
                row=new Vector();               
           }
        }
        return matriz;
    }

    public Set<Alumno> getAlumnos() {
        return alumnos;
    }

    
}
