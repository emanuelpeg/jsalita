/*
 * Trabajo.java
 *
 * Created on 6 de agosto de 2006, 15:15
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package Negocio;

import java.util.Date;
import java.util.SimpleTimeZone;
import java.util.TimeZone;

/**
 *
 * @author PEG
 */
public class Trabajo {
    
    /*Id para Hibernate*/
    private Long id;
    
    /**Descripción */
    private String descripcion;
    
    /**Horario de Entrada */
    private Date entrada;
    
    /**Horario de Salida */
    private Date salida;
    
    /**Dia de la Semana que comienza a trabajar*/
    private String comienzoTrabajo;
    
    /**Dia de la Semana que Finaliza el trabajo*/
    private String finTrabajo;
    
    /**Dirección del Trabajo*/
    private String direccion;
    
    /**Teléfono del Trabajo*/
    private String telefono;
    
    /** Creates a new instance of Trabajo utilizado por hibernate*/
    public Trabajo() {
    }

    /** Constructor por defecto es necesario para crear una instancia, hora de entrada y de salida 
     * como día de comienzo y salida obligatorio */
    public Trabajo(Date entrada, Date salida, String comienzoTrabajo, String finTrabajo){
        this.setEntrada(entrada);
        this.setSalida(salida);
        this.setComienzoTrabajo(comienzoTrabajo);
        this.setFinTrabajo(finTrabajo);
    }
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String Descripcion) {
        this.descripcion = Descripcion;
    }

    public Date getEntrada() {
        return entrada;
    }

    public void setEntrada(Date entrada) {
        this.entrada = entrada;
    }

    public Date getSalida() {
        return salida;
    }

    public void setSalida(Date salida) {
        this.salida = salida;
    }

    public String getComienzoTrabajo() {
        return comienzoTrabajo;
    }

    public void setComienzoTrabajo(String comienzoTrabajo) {
        this.comienzoTrabajo = comienzoTrabajo;
    }

    public String getFinTrabajo() {
        return finTrabajo;
    }

    public void setFinTrabajo(String finTrabajo) {
        this.finTrabajo = finTrabajo;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String toString() {
        return "De " + this.getComienzoTrabajo() + " a " + this.getFinTrabajo() + " - " + this.getDescripcion();
    }
    

    
}
