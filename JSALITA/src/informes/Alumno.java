/*
 * Alumno.java
 *
 * Created on 24 de enero de 2008, 23:24
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package informes;

import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRSubreport;
import net.sf.jasperreports.engine.JasperManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.view.JasperViewer;

/**
 *
 * @author root
 */
public class Alumno implements Informe {
    
    private Negocio.Alumno alumno=null;
    
    /** Creates a new instance of Alumno */
    public Alumno(Negocio.Alumno alumno) {
        this.setAlumno(alumno);
    }
    
    protected Negocio.Alumno getAlumno() {
        return alumno;
    }

    protected void setAlumno(Negocio.Alumno alumno) {
        this.alumno = alumno;
    }

    public void build() throws Exception {
        Map parametros = new HashMap();
        parametros.put("SALA_NOMBRE",this.getAlumno().getSala().getNombre());
        parametros.put("SALA_ANIO",String.valueOf(this.getAlumno().getSala().getAnio()));
        parametros.put("SALA_LETRA",String.valueOf(this.getAlumno().getSala().getLetra()));
        parametros.put("ALUMNO_NOMBRE",this.getAlumno().getNombre());
        parametros.put("ALUMNO_APELLIDO",this.getAlumno().getApellido());
        parametros.put("ALUMNO_DNI",String.valueOf(this.getAlumno().getDni()));
        parametros.put("ALUMNO_DIRECCION",this.getAlumno().getDireccion());
        parametros.put("ALUMNO_ENFERMEDAD",this.getAlumno().getEnfermedad());
        parametros.put("ALUMNO_PARTO",this.getAlumno().getParto());
        parametros.put("ALUMNO_RELIGION",this.getAlumno().getReligion());
        parametros.put("ALUMNO_TELEFONO",this.getAlumno().getTelefono());
        if (this.getAlumno().getSexo()=='M'){
            parametros.put("ALUMNO_SEXO","Masculino");
        } else {
            parametros.put("ALUMNO_SEXO","Femenino");
        }
        SimpleDateFormat sdf= new SimpleDateFormat("dd/MM/yyyy");
        parametros.put("ALUMNO_FNAC",sdf.format(this.getAlumno().getFechaNacimiento()));
        parametros.put("ALUMNO_EDAD",String.valueOf(this.getAlumno().getEdad()));
        JRBeanCollectionDataSource ds = new JRBeanCollectionDataSource(this.getAlumno().getNotas());
        parametros.put("ALUMNO_NOTAS",ds);
        InputStream file = this.getClass().getResourceAsStream("/informes/recursos/Alumno.jasper");
        try {
            
            JasperPrint print = JasperManager.fillReport(file,parametros,ds);           
            JasperViewer jviewer = new JasperViewer(print,false);
            jviewer.show();
        } catch (JRException ex) {
            ex.printStackTrace();
            throw new Exception("Error al imprimir el informe");
        }
    }
    
    
    
}
