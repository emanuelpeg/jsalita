/*
 * Sala.java
 *
 * Created on 24 de enero de 2008, 20:29
 *
 *  Clase generadora del informe Sala
 */

package informes;

import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.view.JasperViewer;

/**
 *
 * @author root
 */
public class Sala implements Informe {
    
    private Negocio.Sala sala=null;
    
    /** Creates a new instance of Sala */
    public Sala(Negocio.Sala sala) {
        this.sala=sala;
    }
    
    
    /**
     * Generar reporte 
     */
    public void build() throws Exception{
        Map parametros = new HashMap();
        parametros.put("SALA_NOMBRE",this.getSala().getNombre());
        parametros.put("SALA_ANIO",String.valueOf(this.getSala().getAnio()));
        parametros.put("SALA_LETRA",String.valueOf(this.getSala().getLetra()));
        InputStream file = this.getClass().getResourceAsStream("/informes/recursos/Salita.jasper");
        try {
            JRBeanCollectionDataSource ds = new JRBeanCollectionDataSource(this.getSala().getAlumnos());
            JasperPrint print = JasperManager.fillReport(file,parametros,ds);
            JasperViewer jviewer = new JasperViewer(print,false);
            jviewer.show();
        } catch (JRException ex) {
            ex.printStackTrace();
            throw new Exception("Error al imprimir el informe");
        }
    }

    protected Negocio.Sala getSala() {
        return sala;
    }

    protected void setSala(Negocio.Sala sala) {
        this.sala = sala;
    }
    
}
