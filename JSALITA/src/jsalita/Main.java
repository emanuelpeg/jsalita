/*
 * Main.java
 *
 * Created on 10 de junio de 2006, 1:20
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package jsalita;

import Interfaz.JSala;
import java.io.FileNotFoundException;
import java.io.IOException;



/**
 *
 * @author usuario
 */
public class Main {
    
    /** Creates a new instance of Main */
    public Main() {
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
       JSala form=new JSala();
       String dir ="conf.interfaz";
       try{
           Interfaz.GuardadorInterfaz gi=new Interfaz.GuardadorInterfaz(dir);
           if(gi.existConfig()){
               Interfaz.Configuracion c = gi.getConfiguracionInterfaz();
               if (c.getLookAndFeel().equals("javax.swing.plaf.metal.MetalLookAndFeel")){
                   Class lnfClass =Class.forName(c.getTheme(), true, Thread.currentThread().getContextClassLoader());
                   javax.swing.plaf.metal.MetalLookAndFeel.setCurrentTheme((javax.swing.plaf.metal.MetalTheme)lnfClass.newInstance());
               }
               form.setLookAndFeel(c.getLookAndFeel());
           }
           form.setDefaultLookAndFeelDecorated(true);
           form.setVisible(true);
       } catch(Exception e){
           mesajeError(e);
       } 
    }
    
    
    private static void mesajeError(Exception e){
           System.out.println("Error:   ");
           System.out.println(e.getMessage());
           System.out.println("Causa:   ");
           System.out.println(e.getCause());
           System.out.println("StackTrace:   ");
           e.printStackTrace();
    }
    
}
