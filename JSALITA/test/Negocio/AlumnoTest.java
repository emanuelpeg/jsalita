/*
 * AlumnoTest.java
 * JUnit based test
 *
 * Created on 10 de junio de 2006, 2:57
 */

package Negocio;

import java.util.GregorianCalendar;
import junit.framework.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;

/**
 *
 * @author usuario
 */
public class AlumnoTest extends TestCase {
    
    public AlumnoTest(String testName) {
        super(testName);
    }

    protected void setUp() throws Exception {
    }

    protected void tearDown() throws Exception {
    }

    public static Test suite() {
        TestSuite suite = new TestSuite(AlumnoTest.class);
        
        return suite;
    }

    /**
     * Test of getFechaSecretario method, of class Negocio.Alumno.
     */

    public void testAsisti() {
//        System.out.println("Entra a asisti");
//        
//        Date fecha = new GregorianCalendar(2000,11,1).getTime();
//        java.text.SimpleDateFormat sdf=new java.text.SimpleDateFormat("dd-MM-yyyy");
//        Alumno instance = new Alumno(29520696,"Pablo","Goette",new Date(2001-1900,06,12),"urquiza",'m');
//        
//        for (int i =1;i<30;i++){
//            fecha.setDate(i);
//            instance.getFaltas().add(fecha);
//            System.out.println(sdf.format(fecha));
//        }
//        
//        
//        boolean result = instance.falte(new Date());
//
//        
//        if (result){
//                fail("The test case is empty.");
//        }
    }

    /**
     * Test of promedio method, of class Negocio.Alumno.
     */
    public void testPromedio() {
        System.out.println("promedio");
        
        Alumno instance = new Alumno(29520696,"Pablo","Goette",new Date(2001-1900,06,12),"urquiza",'m');
        for (int i=1;i<101;i++){
            instance.getNotas().add(new Nota(i,"hola",new Date()));
        }
        double expResult = 50.5;
        double result = instance.promedio();
        System.out.println(result);
        if(expResult!=result) fail("The test case is empty.");
    }
    
}
