/*
 * SalaTest.java
 * JUnit based test
 *
 * Created on 16 de enero de 2005, 14:17
 */

package Negocio;

import java.util.Calendar;
import java.util.GregorianCalendar;
import junit.framework.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.Random;

/**
 *
 * @author Emanuel
 */
public class SalaTest extends TestCase {
    
    Sala sala=new Sala("sala",'L',2006);
    
    public SalaTest(String testName) {
        super(testName);
    }

    protected void setUp() throws Exception {
        Alumno alumno;
        for(int i=0;i<1000;i++){
            alumno=new Alumno(29520696+i,"Emanuel","Goette",new Date(998,01,18+i),"urquiza 645",'M');
            alumno.getFaltas().add(new Date(1000,01,01));
            alumno.setFechaSecretario(new Date(1000,01,01+i));
            for(int j=0;j<10;j++){
                alumno.getNotas().add(new Nota(j,"la nota",new Date(1000,01,01+j)));
            }
            sala.getAlumnos().add(alumno);            
        }
    }

    protected void tearDown() throws Exception {
    }

    public static Test suite() {
        TestSuite suite = new TestSuite(SalaTest.class);
        
        return suite;
    }

    /**
     * Test of getId method, of class Negocio.Sala.
     */
    public void MostrarDatos() {
//        System.out.println("---Ver datos--");
//        for (Alumno alumno:sala.getAlumnos()){
//            System.out.println(alumno.getNombre()+"  "+String.valueOf(alumno.promedio()));
//        }
    }

    /**
     * Test of promedio method, of class Negocio.Sala.
     */
    public void testPromedio() {
        System.out.println("promedio");    
        
        double expResult = 4.5;
        double result = sala.promedio();
        assertEquals(expResult, result);
        if (result==expResult){
            System.out.println("Anda bien el promedio es : "+String.valueOf(result));
        } else{
            System.out.print("result = "+String.valueOf(result));
            System.out.print("expResult = "+String.valueOf(expResult));
            fail("The test case is empty.");
        }
       
    }

    /**
     * Test of elegirSecretario method, of class Negocio.Sala.
     */
    public void testElegirSecretario() {
        System.out.println("elegirSecretario");
        for(int i=0;i<100;i++){
            Alumno alumno=sala.elegirSecretario();
            System.out.println(alumno);
        }
        
        //fail("The test case is empty.");
    }

    /**
     * Test of ausente method, of class Negocio.Sala.
     */
    public void testAusente() {
        System.out.println("ausente");
        for(Alumno alumno:sala.getAlumnos()){
            if (!sala.ausente(alumno,new GregorianCalendar().getTime())){
                fail("The test case is empty.");
            }
        }
        for(Alumno alumno:sala.getAlumnos()){
            if (!sala.ausente(alumno,new GregorianCalendar(2000,01,01).getTime())){
                fail("The test case is empty.");
            }
        }       
        // TODO add your test code below by replacing the default call to fail.
//        fail("The test case is empty.");
    }

    /**
     * Test of removerFalta method, of class Negocio.Sala.
     */
    public void testRemoverFalta() {
        System.out.println("removerFalta");
        
       
    }

    /**
     * Test of calificar method, of class Negocio.Sala.
     */
//    public void testCalificar() {
//        System.out.println("calificar");
//        
//        Alumno palumno = null;
//        Nota nota = null;
//        Sala instance = new Sala();
//        
//        boolean expResult = true;
//        boolean result = instance.calificar(palumno, nota);
//        assertEquals(expResult, result);
//        
//        // TODO add your test code below by replacing the default call to fail.
//        fail("The test case is empty.");
//    }

    /**
     * Test of removerNota method, of class Negocio.Sala.
     */
//    public void testRemoverNota() {
//        System.out.println("removerNota");
//        
//        Alumno palumno = null;
//        Nota nota = null;
//        Sala instance = new Sala();
//        
//        boolean expResult = true;
//        boolean result = instance.removerNota(palumno, nota);
//        assertEquals(expResult, result);
//        
//        // TODO add your test code below by replacing the default call to fail.
//        fail("The test case is empty.");
//    }

    /**
     * Test of listaPresentes method, of class Negocio.Sala.
     */
    public void testListaPresentes() {
        System.out.println("listaPresentes -------------------------");
        
        Date dia = new GregorianCalendar().getTime();
        for(Alumno alumno:sala.getAlumnos()){
            if (!sala.ausente(alumno,new GregorianCalendar(2000,02,03).getTime())){
                fail("The test case is empty.");
            }
        }
        HashSet<Alumno> result = sala.listaPresentes(dia);
        for(Alumno alumno:result){
            System.out.println(alumno);
        }
        // TODO add your test code below by replacing the default call to fail.
        //fail("The test case is empty.");
    }

    /**
     * Test of listarAusentes method, of class Negocio.Sala.
     */
    public void testListarAusentes() {
        System.out.println("listarAusentes");
        
        Date dia = new GregorianCalendar().getTime();
        for(Alumno alumno:sala.getAlumnos()){
            if (!sala.ausente(alumno,new GregorianCalendar(2000,02,01).getTime())){
                fail("The test case is empty.");
            }
        }
        HashSet<Alumno> result = sala.listarAusentes(dia);
        for(Alumno alumno:result){
            System.out.println(alumno);
        }
        
        // TODO add your test code below by replacing the default call to fail.
//        fail("The test case is empty.");
    }
    
}
